import flask
import logging
# Import the blueprint of the child app
from apps.hello_world.hello_world import hello_world

flask_app = flask.Flask(__name__,
                        template_folder="res/templates", # global templates
                        static_folder="res/static")      # global static files

# Register the blueprint of the child app.
# You must provide url_prefix so that Flask can expose the static files provided
# by the blueprint.
flask_app.register_blueprint(hello_world, url_prefix="/api/hello_world")
