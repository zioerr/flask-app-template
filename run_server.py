import main_app

# Serve the Flask apps on port 8090.
main_app.flask_app.run(host="0.0.0.0", port=8089)
