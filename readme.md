# Overview

Flask HTTP App Template.

## Structure

```
root
|
|- res                # main app resources
|  |- static          # main app static files
|  |- templates       # main app templates files
|
|- apps               # child apps via flask blueprints
|  |
|  |- hello_world     # example app: hello_world
|     |- res          # resources for the hello_world app
|        |- static   
|        |- templates
|
|- main_app.py        # setup flask main app and child apps.
|- run_server.py      # serve the apps via http://*:8089
|- requirements.txt   # the app's python requirements
|- tests              # unit tests for child apps (uses pytest)
```