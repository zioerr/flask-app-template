import flask
import logging

_logger = logging.getLogger(__name__)

end_point = "hello_world"

hello_world = flask.Blueprint(end_point, __name__,
                              # template files of this blueprint
                              template_folder='res/templates',
                              # static files of this blueprint
                              static_folder='res/static',
                              # Set url_prefix so that static files are exposed
                              # at <prefix>/static, and functions are exposed
                              # at <prefix>/<functions>
                              url_prefix="/%s" % end_point)


def say_hello(message=None):
    """Returns message as a dictionary"""
    if message is None:
        m = "Hello World"
    else:
        m = message
    return {"message": m}


@hello_world.route("/hello", methods=["GET"])
@hello_world.route("/hello/<message>", methods=["GET"])
def say_json_hello(message=None):
    data = say_hello(message)
    return flask.make_response(flask.jsonify(data), 200)


@hello_world.route("/html/hello", methods=["GET"])
@hello_world.route("/html/hello/<message>", methods=["GET"])
def say_html_hello(message=None):
    data = say_hello(message)
    return flask.render_template('index.html', **data)
