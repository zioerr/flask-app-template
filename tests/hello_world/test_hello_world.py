import pytest
import main_app


@pytest.fixture
def client():
    main_app.flask_app.config['TESTING'] = True
    return main_app.flask_app.test_client()


def test_default_hello(client):
    rv = client.get('/api/hello_world/hello')
    response_content = rv.get_json()
    assert 'message' in response_content
    message = response_content['message']
    assert message == 'Hello World'


def test_custom_hello(client):
    rv = client.get('/api/hello_world/hello/Hi World')
    response_content = rv.get_json()
    assert 'message' in response_content
    message = response_content['message']
    assert message == 'Hi World'
